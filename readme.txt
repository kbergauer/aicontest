The goal of this project is to provide a basic framework in which AI contests can be held.
The master branch will contain the framework used by all agents while each agent should be developed in a separate branch to avoid cheating.
Furthermore, each contestant should use a separate namespace for agent-specific implementations.

At certain intervalls all agent branches will be merged to the master and a tournament will be held.